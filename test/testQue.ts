import { ethers } from "@nomiclabs/buidler";
import chai from "chai";
import { solidity } from "ethereum-waffle";
import { Contract } from "ethers";

chai.use(solidity);
const { expect } = chai;

describe("Que", () => {
  const signer = ethers.provider.getSigner();

  let registry: Contract;
  let que: Contract;

  beforeEach(async () => {
    let controllers = [await signer.getAddress()];

    const queFactory = await ethers.getContract("CapTableQue");
    que = await queFactory.deploy(controllers);

    // que must be controller of registry
    controllers.push(que.address);
    const registryFactory = await ethers.getContract("CapTableRegistry");
    registry = await registryFactory.deploy(controllers);

    const isUserController = await registry.isController(que.address);
    expect(isUserController).to.be.true;

    await que.setRegistry(registry.address);
    expect(registry.address).to.properAddress;
  });

  it("Add to que", async () => {
    const randomAddress = await ethers.Wallet.createRandom().getAddress();
    const tx = await que.functions.add(randomAddress);
    const list = await que.list();
    const listQued = await que.listQued();
    expect(list.length).to.eq(1);
    expect(listQued.length).to.eq(1);
  });
});
