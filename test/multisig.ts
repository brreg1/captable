import BUIDLER, { ethers } from '@nomiclabs/buidler';
import chai from 'chai';
import { solidity } from 'ethereum-waffle';
import { actors } from './utils/actors';

import { CapTable } from '../dist/CapTable';
import { CapTableFactory } from '../dist/CapTableFactory';

import { MultiSigWallet } from '../dist/MultiSigWallet';
import { MultiSigWalletFactory } from '../dist/MultiSigWalletFactory';

chai.use(solidity);
const { expect } = chai;

describe('--## MULTISIG ##--', () => {
    // SIGNERS

    const ownerSigner = new ethers.Wallet(actors.owner.pk).connect(BUIDLER.ethers.provider);
    const partnerSigner = new ethers.Wallet(actors.partner.pk).connect(BUIDLER.ethers.provider);

    // INFO
    let networkId = '';

    // CONTRACTS
    let capTableAsOwner: CapTable;
    let multisigAsOwner: MultiSigWallet;

    before('get network id', async () => {
        networkId = (await BUIDLER.ethers.provider.getNetwork()).chainId.toString();
    });

    it('create capTable', async () => {
        const { queAddress } = await BUIDLER.run('deploy');
        const erc1820Address = BUIDLER.config.deployed[networkId].erc1820;

        capTableAsOwner = await new CapTableFactory(ownerSigner).deploy(
            'Multisig test company',
            'msCO',
            1,
            ['0xC9901c379E672912D86D12Cb8f182cFaf5951940'],
            '0xC8864Ec7b816EB035b732635392405ab8FCaFC17',
            false,
            ['0x4100000000000000000000000000000000000000000000000000000000000000'],
            queAddress,
            erc1820Address,
            ethers.utils.formatBytes32String('123321123'),
        );

        console.log("REACT_APP_CAP_TABLE_ADDRESS='" + capTableAsOwner.address + "'");
        expect(capTableAsOwner.address).to.properAddress;
    });

    it('create multisig', async () => {
        multisigAsOwner = await new MultiSigWalletFactory(ownerSigner).deploy([ownerSigner.address, partnerSigner.address], 2);
        await multisigAsOwner.deployed();

        const transferOwnership = await capTableAsOwner.transferOwnership(multisigAsOwner.address);
        await transferOwnership.wait();
        console.log('Ownership of ', capTableAsOwner.address, 'is transfered to multisig', multisigAsOwner.address);
    });

    it('check if capTableOwner is multisig', async () => {
        const ERC1820_ACCEPT_MAGIC = ethers.utils.solidityKeccak256(['string'], ['ERC1820_ACCEPT_MAGIC']);
        const ERC1400_MULTISIG = ethers.utils.solidityKeccak256(['string'], ['ERC1400Multisig']);
        let implementsInterface = false;
        try {
            const bytesAcceptMagicMaybe = await multisigAsOwner.canImplementInterfaceForAddress(ERC1400_MULTISIG, ethers.constants.AddressZero);
            if (bytesAcceptMagicMaybe === ERC1820_ACCEPT_MAGIC) {
                implementsInterface = true;
            }
        } catch (error) {}

        expect(implementsInterface).to.be.true;
    });
});
