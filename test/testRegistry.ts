import { ethers } from "@nomiclabs/buidler";
import chai from "chai";
import { solidity } from "ethereum-waffle";
import { Contract } from "ethers";

chai.use(solidity);
const { expect } = chai;

describe("registry", () => {
  const signer = ethers.provider.getSigner();

  let registry: Contract;

  beforeEach(async () => {
    const controllers = [await signer.getAddress()];
    const registryFactory = await ethers.getContract("CapTableRegistry");
    registry = await registryFactory.deploy(controllers);

    expect(registry.address).to.properAddress;
  });

  it("Add to registry", async () => {
    const randomAddress = await ethers.Wallet.createRandom().getAddress();
    await registry.add(randomAddress);
    const list = await registry.list();
    expect(list.length).to.eq(1);
  });
});
