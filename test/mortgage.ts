import { ethers, tasks, config } from '@nomiclabs/buidler';
import BRE from '@nomiclabs/buidler';
import chai from 'chai';
import { solidity } from 'ethereum-waffle';

import { CapTable } from '../dist/CapTable';
import { IAuthOracle } from '../dist/IAuthOracle';
import { ERC1820RegistryDeployable } from '../dist/ERC1820RegistryDeployable';
import { ERC20Mintable } from '../dist/ERC20Mintable';
import { PropertyCDP } from '../dist/PropertyCDP';
import { PropertyTrade } from '../dist/PropertyTrade';

import { actors } from './utils/actors';
import { getContractFactory } from '@nomiclabs/buidler/types';
import { Contract } from 'ethers';
import { NumToNumBytes32, NumToHexBytes32, addressToBytes32 } from './utils/dataBytes32Format';

chai.use(solidity);
const { expect } = chai;

describe('mortgage', () => {
    // SIGNERS
    const signer = ethers.provider.getSigner();
    signer.getAddress().then(signerAddress => console.log('@signer', signerAddress));
    const managerSigner = ethers.Wallet.createRandom().connect(signer.provider);
    console.log('@Manager=>', managerSigner.address);
    const sellerSigner = ethers.Wallet.createRandom().connect(signer.provider);
    const authOracleSigner = new ethers.Wallet(actors.authOracle.pk).connect(signer.provider);
    const bankSigner = new ethers.Wallet(actors.bank.pk).connect(signer.provider);
    const buyerSigner = new ethers.Wallet(actors.buyer.pk).connect(signer.provider);

    // INFO
    let networkId = '';
    let capTableAddress = '';
    const PARTITION = ethers.utils.formatBytes32String('A-AKSJE');
    const SHARE_VALUE = 3200000;
    const SHARE_AMOUNT = 1;
    const SHARE_VALUE_AFTER_SALE = 4600000;

    // CONTRACTS
    let capTableAsManager: CapTable;
    let propertyCDPForSellerAsManager: PropertyCDP;
    let propertyTradeAsSeller: PropertyTrade;
    let tokenValidator: Contract;

    before('get network id', async () => {
        networkId = (await ethers.provider.getNetwork()).chainId.toString();
    });

    it('create erc1400', async () => {
        const { queAddress } = await BRE.run('deploy');
        const erc1820Address = BRE.config.deployed[networkId].erc1820;

        const capTableFactory = await ethers.getContractFactory('CapTable', managerSigner);
        const capTableArgs = [
            'Mortgage Test Company',
            'MORT',
            1,
            ['0xC9901c379E672912D86D12Cb8f182cFaf5951940'],
            '0xC8864Ec7b816EB035b732635392405ab8FCaFC17',
            false,
            ['0x4100000000000000000000000000000000000000000000000000000000000000'],
            queAddress,
            erc1820Address,
            ethers.utils.formatBytes32String('123'),
        ];
        capTableAsManager = (await capTableFactory.deploy(...capTableArgs)) as CapTable;
        capTableAddress = capTableAsManager.address;
        console.log("REACT_APP_CAP_TABLE_ADDRESS='" + capTableAsManager.address + "'");
        expect(capTableAsManager.address).to.properAddress;
    });

    it('capTable set tokenValidator', async () => {
        expect(await capTableAsManager.isOwner()).to.be.true;
        const tokenValidatorFactory = await ethers.getContractFactory('ERC1400BrregValidator', managerSigner);
        const authOracleAddress = BRE.config.deployed[networkId].authOracle;
        tokenValidator = await tokenValidatorFactory.deploy(authOracleAddress);
        console.log('Token Validator', tokenValidator.address);
        console.log('Auth oracle is ', authOracleAddress);

        const setHookContract = await capTableAsManager.setHookContract(tokenValidator.address, 'ERC1400TokensValidator');
        await setHookContract.wait();
    });

    it('Managers issues shares for Seller to him/herself', async () => {
        // const address;
        const issueTX = await capTableAsManager.issueByPartition(PARTITION, managerSigner.address, SHARE_AMOUNT, '0x3300000000000000000000000000000000000000000000000000000000000000');
        await issueTX.wait();
        const balance = await capTableAsManager.balanceOf(managerSigner.address);
        expect(balance.toNumber()).to.be.greaterThan(0);
    });

    it('manager creates seller CDP', async () => {
        const propertyCDPFactorty = await ethers.getContractFactory('PropertyCDP', managerSigner);
        const erc1820 = BRE.config.deployed[networkId].erc1820;
        propertyCDPForSellerAsManager = (await propertyCDPFactorty.deploy(erc1820)) as PropertyCDP;
        console.log('@sellerCDP =>', propertyCDPForSellerAsManager.address);

        expect(propertyCDPForSellerAsManager.address).to.be.properAddress;
    });

    // TODO: CDP auth should not be needed
    it('authOracle authtenticates seller& CDP', async () => {
        const factory = await ethers.getContractFactory('IAuthOracle', authOracleSigner);
        let authOracleAddress = BRE.config.deployed[networkId].authOracle;
        const authOracle = factory.attach(authOracleAddress) as IAuthOracle;

        const setSeller = await authOracle.set_ssn_to_address(sellerSigner.address, ethers.utils.formatBytes32String('201288xxxxx'), ethers.utils.formatBytes32String('Selje Seller'));
        await setSeller.wait();
        const isSellerAuthenticated = await authOracle.is_address_attested(sellerSigner.address);
        expect(isSellerAuthenticated).to.be.true;

        const setCDP = await authOracle.set_ssn_to_address(propertyCDPForSellerAsManager.address, ethers.utils.formatBytes32String('SC201288xxxxx'), ethers.utils.formatBytes32String('Selje Seller CDP'));
        await setCDP.wait();
        const isCDPAuthenticated = await authOracle.is_address_attested(propertyCDPForSellerAsManager.address);
        expect(isCDPAuthenticated).to.be.true;
    });

    xit('manager transfers shares to sellerCDP and receives cToken and pToken', async () => {
        const transferByPartition = await capTableAsManager.transferByPartition(
            PARTITION,
            propertyCDPForSellerAsManager.address,
            SHARE_AMOUNT,
            '0x4341505f5441424c455f4344505f464c41470000000000000000000000000000' + NumToHexBytes32(SHARE_VALUE),
        );
        await transferByPartition.wait();
        const CDPBalance = await capTableAsManager.balanceOf(propertyCDPForSellerAsManager.address);
        const managerBalance = await capTableAsManager.balanceOf(managerSigner.address);
        console.log('CDP balance => ', CDPBalance.toNumber());
        console.log('Manager balance => ', managerBalance.toNumber());
    });

    xit('manager possesses ' + SHARE_AMOUNT + ' cBolig and ' + SHARE_VALUE + ' pTokens', async () => {
        const cTokenAddress = await propertyCDPForSellerAsManager.getCToken();
        const pTokenAddress = await propertyCDPForSellerAsManager.getPToken();
        console.log('cTokenAddress', cTokenAddress);
        console.log('pTokenAddress', pTokenAddress);
        const factory = await ethers.getContractFactory('ERC20Mintable', managerSigner);
        const cToken = (await factory.attach(cTokenAddress)) as ERC20Mintable;
        const pToken = (await factory.attach(pTokenAddress)) as ERC20Mintable;
        const managerBalanceCToken = await cToken.balanceOf(managerSigner.address);
        const managerBalancePToken = await pToken.balanceOf(managerSigner.address);
        expect(managerBalanceCToken.toNumber()).to.be.eq(SHARE_AMOUNT);
        expect(managerBalancePToken.toNumber()).to.be.eq(SHARE_VALUE);
    });

    it('manager transfers shares to sellerCDP', async () => {
        // TODO - This can just be issued. Create a test for it latger
        const transferByPartition = await capTableAsManager.transferByPartition(
            PARTITION,
            propertyCDPForSellerAsManager.address,
            SHARE_AMOUNT,
            '0x4341505f5441424c455f4344505f464c41470000000000000000000000000000' + NumToHexBytes32(SHARE_VALUE) + addressToBytes32(bankSigner.address) + addressToBytes32(sellerSigner.address),
        );
        await transferByPartition.wait();
        const CDPBalance = await capTableAsManager.balanceOf(propertyCDPForSellerAsManager.address);
        const managerBalance = await capTableAsManager.balanceOf(managerSigner.address);
        console.log('CDP balance => ', CDPBalance.toNumber());
        console.log('Manager balance => ', managerBalance.toNumber());
    });

    it('check ERC1400TokensRecipient implementer', async () => {
        const factory = await ethers.getContractFactory('ERC1820RegistryDeployable', authOracleSigner);
        const erc1820 = BRE.config.deployed[networkId].erc1820;
        const contract = (await factory.attach(erc1820)) as ERC1820RegistryDeployable;

        // Check token recipient
        const ERC1400TokensRecipientHash = ethers.utils.solidityKeccak256(['string'], ['ERC1400TokensRecipient']);
        const tokenRecipientsImplementer = await contract.getInterfaceImplementer(propertyCDPForSellerAsManager.address, ERC1400TokensRecipientHash);
        expect(propertyCDPForSellerAsManager.address).to.be.eq(tokenRecipientsImplementer);
        console.log('Expect CDP address', propertyCDPForSellerAsManager.address);
        console.log('to be ERC1400TokensRecipient implementer ', tokenRecipientsImplementer);
    });

    it('check ERC1400TokensValidator implementer', async () => {
        const factory = await ethers.getContractFactory('ERC1820RegistryDeployable', authOracleSigner);
        const erc1820 = BRE.config.deployed[networkId].erc1820;
        const contract = (await factory.attach(erc1820)) as ERC1820RegistryDeployable;

        // Check token validator
        const ERC1400TokensValidatorHash = ethers.utils.solidityKeccak256(['string'], ['ERC1400TokensValidator']);
        const tokenValidatorImplementer = await contract.getInterfaceImplementer(capTableAddress, ERC1400TokensValidatorHash);
        expect(tokenValidatorImplementer).to.be.eq(tokenValidator.address);
    });

    it('manager transfers / has transfered cToken to seller and pToken to seller bank', async () => {
        const cTokenAddress = await propertyCDPForSellerAsManager.getCToken();
        const pTokenAddress = await propertyCDPForSellerAsManager.getPToken();
        const factory = await ethers.getContractFactory('ERC20Mintable', managerSigner);
        const cToken = (await factory.attach(cTokenAddress)) as ERC20Mintable;
        const pToken = (await factory.attach(pTokenAddress)) as ERC20Mintable;

        // const tx1 = await cToken.transfer(sellerSigner.address, SHARE_AMOUNT);
        // await tx1.wait();

        // const tx2 = await pToken.transfer(bankSigner.address, SHARE_VALUE);
        // await tx2.wait();

        const sellerBalance = await cToken.balanceOf(sellerSigner.address);
        const bankBalance = await pToken.balanceOf(bankSigner.address);

        expect(sellerBalance.toNumber()).to.be.eq(SHARE_AMOUNT);
        expect(bankBalance.toNumber()).to.be.eq(SHARE_VALUE);
    });

    xit('Loan should be marked as priviously granted', async () => {});

    it('seller creates PropertyTrade', async () => {
        const PropertyTradeFactory = await ethers.getContractFactory('PropertyTrade', sellerSigner);
        const erc1820 = BRE.config.deployed[networkId].erc1820;
        propertyTradeAsSeller = (await PropertyTradeFactory.deploy(erc1820, propertyCDPForSellerAsManager.address)) as PropertyTrade;
        console.log('@propertyTradeAsSeller =>', propertyTradeAsSeller.address);
        const totalSupplyCToken = await propertyTradeAsSeller.getCTokenTotalSupply();
        const totalSupplyPToken = await propertyTradeAsSeller.getPTokenTotalSupply();
        expect(totalSupplyCToken).to.be.eq(SHARE_AMOUNT);
        expect(totalSupplyPToken).to.be.eq(SHARE_VALUE);
    });

    // TODO: should not be needed
    xit('authOracle authtenticates PropertyTrade', async () => {
        const factory = await ethers.getContractFactory('IAuthOracle', authOracleSigner);
        let authOracleAddress = BRE.config.deployed[networkId].authOracle;
        const authOracle = factory.attach(authOracleAddress) as IAuthOracle;

        const authPropertyTrade = await authOracle.set_ssn_to_address(propertyTradeAsSeller.address, ethers.utils.formatBytes32String('010101'), ethers.utils.formatBytes32String('Eierskifte kontrakt'));
        await authPropertyTrade.wait();
        const isPropertyTradeAuthenticated = await authOracle.is_address_attested(propertyTradeAsSeller.address);
        expect(isPropertyTradeAuthenticated).to.be.true;
    });

    it('buyer registers himself', async () => {
        const PropertyTradeFactory = await ethers.getContractFactory('PropertyTrade', buyerSigner);
        const propertyTradeAsBuyer = (await PropertyTradeFactory.attach(propertyTradeAsSeller.address)) as PropertyTrade;

        const tx = await propertyTradeAsBuyer.registerAsBuyer(SHARE_VALUE_AFTER_SALE, await bankSigner.getAddress());
        await tx.wait();
    });

    it('seller locks buyer', async () => {
        const tx = await propertyTradeAsSeller.lockBuyer(await buyerSigner.getAddress());
        await tx.wait();
        expect(await propertyTradeAsSeller.getLockedBuyer()).to.be.eq(await buyerSigner.getAddress());
    });

    it('sellersBank approves Trade Sum and releases pTokens to PropertyTrade', async () => {
        const pTokenAddress = await propertyTradeAsSeller.getPToken();
        const factory = await ethers.getContractFactory('ERC20', bankSigner);
        const pTokenAsBank = (await factory.attach(pTokenAddress)) as ERC20Mintable;
        const bankPTokenSupply = await pTokenAsBank.balanceOf(bankSigner.address);
        const tx = await pTokenAsBank.transfer(propertyTradeAsSeller.address, bankPTokenSupply);
        await tx.wait();
        expect(await (await pTokenAsBank.balanceOf(propertyTradeAsSeller.address)).toNumber()).to.be.eq(bankPTokenSupply.toNumber());
    });

    it('Seller approves cBolig for propertyTrade to eierskifte and confirms him/herself as seller', async () => {
        const cTokenAddress = await propertyCDPForSellerAsManager.getCToken();
        const factory = await ethers.getContractFactory('ERC20Mintable', sellerSigner);
        const cToken = (await factory.attach(cTokenAddress)) as ERC20Mintable;
        const tx = await cToken.approve(propertyTradeAsSeller.address, SHARE_AMOUNT);
        await tx.wait();
        const tx2 = await propertyTradeAsSeller.registerAsSeller();
        await tx2.wait();
        // const tx = await cToken.transfer(propertyTradeAsSeller.address, SHARE_AMOUNT);
        // await tx.wait();
        expect((await cToken.balanceOf(propertyTradeAsSeller.address)).toNumber()).to.be.eq(SHARE_AMOUNT);
        expect(await propertyTradeAsSeller.getSeller()).to.be.eq(sellerSigner.address);
    });

    it('Buyer confirms takeover', async () => {
        const PropertyTradeFactory = await ethers.getContractFactory('PropertyTrade', buyerSigner);
        const propertyTradeAsBuyer = (await PropertyTradeFactory.attach(propertyTradeAsSeller.address)) as PropertyTrade;

        const tx = await propertyTradeAsBuyer.confirmTakeover();
        await tx.wait();
        const takeOverConfirmations = await propertyTradeAsBuyer.getTakeOverConfirmations();
        expect(takeOverConfirmations[1]).to.be.true;
    });

    it('Seller confirms takeover, and should propegate CDP close', async () => {
        const tx = await propertyTradeAsSeller.confirmTakeover();
        await tx.wait();
        const takeOverConfirmations = await propertyTradeAsSeller.getTakeOverConfirmations();
        expect(takeOverConfirmations[0]).to.be.true;
        expect(await propertyTradeAsSeller.closed()).to.be.true;
    });

    it('PropertyTrade creates buyersCDP with ' + SHARE_AMOUNT + ' share, issue cToken & pToken, sends cToken to buyer and pToken to mortgageHolder', async () => {
        const buyerCDPaddress = await propertyTradeAsSeller.getBuyerPropertyCDPAddress();
        expect(buyerCDPaddress).to.be.properAddress;

        const propertyCDPFactory = await ethers.getContractFactory('PropertyCDP', managerSigner);
        const buyerCDP = (await propertyCDPFactory.attach(buyerCDPaddress)) as PropertyCDP;

        const cTokenAddress = await buyerCDP.getCToken();
        const pTokenAddress = await buyerCDP.getPToken();

        const erc20Factory = await ethers.getContractFactory('ERC20Mintable', managerSigner);
        const cToken = (await erc20Factory.attach(cTokenAddress)) as ERC20Mintable;
        const pToken = (await erc20Factory.attach(pTokenAddress)) as ERC20Mintable;

        const buyerBalance = await cToken.balanceOf(buyerSigner.address);
        const bankBalance = await pToken.balanceOf(bankSigner.address);

        expect(buyerBalance.toNumber()).to.be.eq(SHARE_AMOUNT);
        expect(bankBalance.toNumber()).to.be.eq(SHARE_VALUE_AFTER_SALE);
    });
});
