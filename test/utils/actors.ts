export const actors = {
    authOracle: {
        pk: '0xad6f29b5b5285c8137787710ebdcc5ee16a3f09598a798bee470e158ade704fc', // 0xbb1c879cb7f5129ba026DfE1E5f30979D7978A65
        ssn: '01110851509',
        name: 'Auth Orakel',
    },
    partner: {
        pk: '778619ab3966fdfe60de7160740f05c98203d8c05de7492bbe7702a7e411cbcc', // 0xbb1c879cb7f5129ba026DfE1E5f30979D7978A65
        ssn: '03038311111',
        name: 'Partner',
    },
    owner: {
        pk: '992827d0c94e08f5c3edef367ee813095c50c9d129d4e2afa0621282aa747aaf', // 0x112C094afC18ef9bC7eA70B1210691f99ceD50e8
        ssn: '17035318933',
        name: 'Kjell Hagseth',
    },
    seller: {
        pk: '4d80d72ccac49ea50d4aab162c1a33eb5f6d9959e089341cecd5badf00ab88eb', // 0x22a8702697a45E723F40620133D1bA28f777E4a0
        ssn: '20060342955',
        name: 'Sylvia Hoppentoff',
    },
    propertyManager: {
        pk: 'dc43216a6f39ae03d66c41fee9cd8dac61e7d9bcc6ec2fe88e1b1a67af8daf33', // 0x3325Bb1fb8094d8e335aA6d4692e55c6e99681F0
        ssn: '25061640072',
        name: 'Olivia Properla',
    },
    bank: {
        pk: 'e9512928a79a8f99a5524be3431d7bf1f9d1ae31d0465e1b1128bf48093ae5c5', // 0x44E292989fDf642F9A99c8B6B378A3E99E5cC423
        ssn: '28031932191',
        name: 'DNB BANK ASA',
    },
    broker: {
        pk: 'd21494b35c66feee32373342744ddcba16241fd993582ebfc7baeb13e9c602e3', // 0x550Fca9b5C73a3802bD20e9f843270faAbA2f41A
        ssn: '04117807199',
        name: 'Anni Kari Sterten',
    },
    controller: {
        pk: '7a3bbccd03c17f110e59514fb4bec8a1c1565876abfe97f8e5d6f5ef31fa6955', // 0xC9901c379E672912D86D12Cb8f182cFaf5951940
        ssn: '05084817521',
        name: 'Brønnøysundregistrene',
    },
    buyer: {
        pk: '48a8afa4f8c4dbbc0dba25d4f0058be90425ed3159fa688dcececef926367f4f', // 0xb1188e7CdFbAd368F32d12501425810043Eea9c7
        ssn: '29038146902',
        name: 'Benny Kjoppstad',
    },
};
