import { BuidlerConfig, usePlugin } from '@nomiclabs/buidler/config';
import { CapTableTasks } from './scripts/tasks';

usePlugin('@nomiclabs/buidler-waffle');
usePlugin('@nomiclabs/buidler-ethers');
usePlugin('buidler-typechain');

// usePlugin("buidler-gas-reporter");
// usePlugin("@nomiclabs/buidler-solpp");

CapTableTasks();

const config: BuidlerConfig = {
    defaultNetwork: 'local',
    deployed: {
        5777: {
            // ganache
            authOracle: '0x47D79c81c07b810038Eec95455E8D89F2c60c195',
            que: '0x839C6E6799318a4df979039C3993c6de692CF428',
            registry: '0x4648F64214eE1dDae5379450547E307C35d794ca',
            erc1820: '0x24523cAF7569358b61B56D20f67F78Ea1d1C823f',
        },
        1851600620: {
            // grefsen
            authOracle: '0x755A9Ccd4EE424Cf631fE68f9DA203De8d1Ac622',
            que: '0x277720E2aac14a4b9FD0811FaFA90B029AB880fa',
            registry: '0x4995d48a0A71B76E2474C1a8eD1d894Dd3bceaAe',
            erc1820: '0xd45d3000de5f853823DBE812b46BC2842f3a891E',
        },
        31337: {
            // buidlerevm
            authOracle: '0x755A9Ccd4EE424Cf631fE68f9DA203De8d1Ac622',
        },
        1733917133: {
            // brreg
            authOracle: '0x4A6Ecc8631990741263b38EEB434405C04CDc723',
        },
    },
    networks: {
        local: {
            url: ' http://127.0.0.1:8545/',
        },
        buidlerevm: {
            gasPrice: 0,
            gas: 'auto',
            blockGasLimit: 8000000,
            // loggingEnabled: true,
        },
        grefsen: {
            url: 'https://e0qzjtvk2c:F38R7jlOwc71Idz0KO-sOdI91qg0HkzibZBDkufwbQA@e0yoqxkx27-e0kr6bistz-rpc.de0-aws.kaleido.io',
            // gasMultiplier: 2,
            // port: 8545,
            // gas: 5000000,
            gasPrice: 0,
        },
        brreg: {
            url: 'https://u1qdua80h5:Er0LWdZuKqOza22YNQKhtdFCbqRzhzGCRhuZgrtHZ9s@u1txh1ent0-u1ieecy018-rpc.us1-azure.kaleido.io',
            gasPrice: 0,
        },
        rinkeby: {
            url: 'https://rinkeby.infura.io/v3/499c90502ea347c3971243e0e0f7172e',
            accounts: {
                mnemonic: 'gas blush witness leopard voyage regret napkin mail onion pitch gather soon',
            },
        },
    },
    solc: {
        version: '0.5.5',
        optimizer: {
            enabled: true,
            runs: 50,
        },
        // evmVersion: "byzantium",
    },
    typechain: {
        outDir: 'dist/',
        target: 'ethers',
    },
    gasReporter: {
        currency: 'NOK',
        gasPrice: 5,
    },
    // solpp: {
    //   cwd: "contracts",
    //   tolerant: true
    // }
};

export default config;
