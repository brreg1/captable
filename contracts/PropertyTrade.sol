pragma solidity >=0.5.5;

import "./token/ERC20/ERC20MintableBurnable.sol";
import "@openzeppelin/contracts/ownership/Ownable.sol";
import "./token/ERC1400Raw/IERC1400TokensRecipient.sol";
import "./token/ERC1820/ERC1820Implementer.sol";
import "./token/ERC1820/ERC1820Client.sol";
import "./PropertyCDP.sol";
import "./token/ERC1400Partition/IERC1400Partition.sol";


contract PropertyTrade is
    Ownable,
    IERC1400TokensRecipient,
    ERC1820Implementer,
    ERC1820Client
{
    string internal constant ERC1400_TOKENS_RECIPIENT = "ERC1400TokensRecipient";
    string internal constant ERC20_TOKENS_RECIPIENT = "ERC20TokensRecipient";
    string internal constant ERC20_PROPERTY_TRADE = "ERC1400PropertyTrade";

    bytes32 internal constant CAP_TABLE_CDP_FLAG = 0x4341505f5441424c455f4344505f464c41470000000000000000000000000000;

    struct PotentialBuyer {
        uint256 mortgageValue;
        address mortgageHolder;
    }
    mapping(address => PotentialBuyer) internal _potentialBuyers;
    address _lockedBuyer;
    address _seller;

    PropertyCDP internal _sellerPropertyCDP;
    PropertyCDP internal _buyerPropertyCDP;
    bool[2] internal _takeOverConfirmations; // 1 seller, 2 buyer

    constructor(address erc1820, address cdpAddress)
        public
        ERC1820Client(erc1820)
    {
        ERC1820Implementer._setInterface(ERC1400_TOKENS_RECIPIENT);
        ERC1820Implementer._setInterface(ERC20_TOKENS_RECIPIENT);
        ERC1820Implementer._setInterface(ERC20_PROPERTY_TRADE);
        ERC1820Client.setInterfaceImplementation(
            ERC1400_TOKENS_RECIPIENT,
            address(this)
        );
        _sellerPropertyCDP = PropertyCDP(cdpAddress);
    }

    function closed() external view returns (bool) {
        return _sellerPropertyCDP.closed();
    }

    function getPTokenTotalSupply() external view returns (uint256) {
        return _getPTokenTotalSupply();
    }

    function getCTokenTotalSupply() external view returns (uint256) {
        return _getCTokenTotalSupply();
    }

    function _getPTokenTotalSupply() internal view returns (uint256) {
        return _getPToken().totalSupply();
    }

    function _getCTokenTotalSupply() internal view returns (uint256) {
        return _getCToken().totalSupply();
    }

    function _getPTokenBalance(address adr) internal view returns (uint256) {
        return _getPToken().balanceOf(adr);
    }

    function _getCTokenBalance(address adr) internal view returns (uint256) {
        return _getCToken().balanceOf(adr);
    }

    function hasCTokens() external view returns (bool) {
        return _hasCTokens();
    }

    function _hasCTokens() internal view returns (bool) {
        if (_getCTokenBalance(address(this)) == _getCTokenTotalSupply()) {
            return true;
        }
        return false;
    }

    function hasPTokens() external view returns (bool) {
        return _hasPTokens();
    }

    function _hasPTokens() internal view returns (bool) {
        if (_getPTokenBalance(address(this)) == _getPTokenTotalSupply()) {
            return true;
        }
        return false;
    }

    function _getCToken() internal view returns (ERC20MintableBurnable) {
        address cTokenAddress = _sellerPropertyCDP.getCToken();
        return ERC20MintableBurnable(cTokenAddress);
    }

    function getCToken() external view returns (ERC20MintableBurnable) {
        return _getCToken();
    }

    function _getPToken() internal view returns (ERC20MintableBurnable) {
        address pTokenAddress = _sellerPropertyCDP.getPToken();
        return ERC20MintableBurnable(pTokenAddress);
    }

    function getPToken() external view returns (ERC20MintableBurnable) {
        return _getPToken();
    }

    function getSellerPropertyCDPAddress() external view returns (address) {
        return address(_sellerPropertyCDP);
    }

    function getBuyerPropertyCDPAddress() external view returns (address) {
        return address(_buyerPropertyCDP);
    }

    function getTakeOverConfirmations() external view returns (bool[2] memory) {
        return _takeOverConfirmations;
    }

    function registerAsSeller() external {
        // TODO: Check for voulnaribilties
        require(
            _getCToken().transferFrom(msg.sender, address(this), 1),
            "Approve cToken for propertyTrade first"
        );
        require(
            _getCToken().balanceOf(address(this)) == 1,
            "cToken balance was not correct"
        );
        _seller = msg.sender;
    }

    function getSeller() external view returns (address) {
        return _seller;
    }

    function registerAsBuyer(uint256 mortgageValue, address mortgageHolder)
        external
    {
        // TODO: Create some kind of lock so one its not possible to change mortgage right before takeover
        // TODO: Create some kind of restriction so not anyone can prupose mortgage.
        PotentialBuyer memory potentialBuyer = PotentialBuyer({
            mortgageValue: mortgageValue,
            mortgageHolder: mortgageHolder
        });
        _potentialBuyers[msg.sender] = potentialBuyer;
    }

    function lockBuyer(address buyerAddress) external {
        require(
            _potentialBuyers[buyerAddress].mortgageHolder != address(0),
            "Cant lock buyer with 0 address, buyer probably not registered."
        );
        _lockedBuyer = buyerAddress;
    }

    function getLockedBuyer() external view returns (address) {
        return _lockedBuyer;
    }

    function confirmTakeover() external {
        require(_hasPTokens(), "Contract must possess all pTokens");
        require(_hasCTokens(), "Contract must possess all cTokens");
        require(_lockedBuyer != address(0), "Buyer cannot be 0 address");
        require(_seller != address(0), "Seller cannot be 0 address");
        if (_seller == msg.sender) {
            _takeOverConfirmations[0] = true;
        }
        if (_lockedBuyer == msg.sender) {
            _takeOverConfirmations[1] = true;
        }

        if (_takeOverConfirmations[0] && _takeOverConfirmations[1]) {
            _getCToken().burn(_getCTokenTotalSupply());
            _getPToken().burn(_getPTokenTotalSupply());
            require(
                _sellerPropertyCDP.closeCDP(address(this)),
                "Close CDP did not complete"
            );
            // Because we are telling CDP to close and transfer the token to ourself. This function will now continue in tokensReceived as it will be called form ERC1400.transferByPartition
        }
    }

    function canReceive(
        bytes4, /*functionSig*/
        bytes32, /*partition*/
        address, /*operator*/
        address from,
        address to,
        uint256 value,
        bytes calldata data,
        bytes calldata // Comments to avoid compilation warnings for unused variables. /*operatorData*/
    ) external view returns (bool) {
        return (_canReceive(from, to, value, data));
    }

    function tokensReceived(
        bytes4, /*functionSig*/
        bytes32 partition,
        address, /*operator*/
        address from,
        address to,
        uint256 value,
        bytes calldata data,
        bytes calldata // Comments to avoid compilation warnings for unused variables. /*operatorData*/
    ) external {
        require(_canReceive(from, to, value, data), "A6"); // Transfer Blocked - Receiver not eligible
        _buyerPropertyCDP = new PropertyCDP(address(ERC1820REGISTRY));
        // require(
        //     address(_buyerPropertyCDP) != address(0),
        //     "Buyer CDP must be valid address"
        // );
        IERC1400Partition(_sellerPropertyCDP.getErc1400ContractAddress())
            .transferByPartition(
            partition,
            address(_buyerPropertyCDP),
            value,
            abi.encode(
                CAP_TABLE_CDP_FLAG,
                _potentialBuyers[_lockedBuyer].mortgageValue
            )
        );
        address buyerCTokenAddress = _buyerPropertyCDP.getCToken();
        address buyerPTokenAddress = _buyerPropertyCDP.getPToken();
        ERC20MintableBurnable(buyerCTokenAddress).transfer(_lockedBuyer, value);
        // TODO : Fix so that value is issued instead
        ERC20MintableBurnable(buyerPTokenAddress).transfer(
            _potentialBuyers[_lockedBuyer].mortgageHolder,
            _potentialBuyers[_lockedBuyer].mortgageValue
        );
    }

    function _canReceive(
        address from,
        address, /*to*/
        uint256, /*value*/
        bytes memory // Comments to avoid compilation warnings for unused variables. /* data */
    ) internal view returns (bool) {
        // We only want to receive ERC1400 tokens from sellerCDP and when confirmTakeover has ran.
        if (from != address(_sellerPropertyCDP)) {
            return false;
        }
        if (!_takeOverConfirmations[0] || !_takeOverConfirmations[1]) {
            return false;
        }
        return true;
    }
}
