pragma solidity >=0.5.5;


interface IAuthOracle {
    event NewSsnToAddress(address indexed adr, bytes32 indexed ssn);

    function suggest_ssn_to_address(bytes32 ssn, bytes32 name) external;

    function set_ssn_to_address(
        address adr,
        bytes32 ssn,
        bytes32 name
    ) external;

    function get_address_from_ssn(bytes32 ssn)
        external
        view
        returns (address adr);

    function get_ssn_from_address(address adr)
        external
        view
        returns (bytes32 ssn);

    function get_name_from_address(address adr)
        external
        view
        returns (bytes32 name);

    function get_name_from_ssn(bytes32 ssn)
        external
        view
        returns (bytes32 name);

    function address_owns_ssn(address adr, bytes32 ssn)
        external
        view
        returns (bool attested);

    function is_address_attested(address adr) external view returns (bool);
}
