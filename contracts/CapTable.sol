pragma solidity >=0.5.5;

import "./IAuthOracle.sol";
import "./ICapTableQue.sol";
import "./token/ERC20/ERC1400ERC20.sol";


contract CapTable is ERC1400ERC20 {
    string internal constant CAP_TABLE_INTERFACE_NAME = "CapTable";
    ICapTableQue internal _capTableQue;
    bytes32 internal _uuid;

    constructor(
        string memory name,
        string memory symbol,
        uint256 granularity,
        address[] memory controllers,
        address certificateSigner,
        bool certificateActivated,
        bytes32[] memory defaultPartitions,
        address capTableQue,
        address erc1820,
        bytes32 uuid
    )
        public
        ERC1400ERC20(
            name,
            symbol,
            granularity,
            controllers,
            certificateSigner,
            certificateActivated,
            defaultPartitions,
            erc1820
        )
    {
        console.log("CapTable::constuctor::START %s", gasleft());

        _capTableQue = ICapTableQue(capTableQue);
        ERC1820Client.setInterfaceImplementation(
            CAP_TABLE_INTERFACE_NAME,
            address(this)
        );

        ERC1820Implementer._setInterface(CAP_TABLE_INTERFACE_NAME); // For migration
        _capTableQue.add(address(this));
        _uuid = uuid;
        console.log("CapTable::constuctor::END %s", gasleft());
    }

    function setUuid(bytes32 uuid) external {
        _uuid = uuid;
    }

    function getUuid() external view returns (bytes32) {
        return _uuid;
    }
}
