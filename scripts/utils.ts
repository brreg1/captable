import { Signer } from "ethers";
import { BigNumber } from "ethers/utils";
import { createLocalStorage } from "localstorage-ponyfill";
import { BuidlerRuntimeEnvironment } from "@nomiclabs/buidler/types";

declare var { ethers }: BuidlerRuntimeEnvironment;

export const saveContractAddress = async (
  contractName: string,
  address: string
) => {
  const localStorage = createLocalStorage();
  const networkId = await getNetworkId();
  localStorage.setItem(networkId + "_" + contractName, address);
};

export const getContractAddress = async (
  contractName: string
): Promise<string> => {
  const localStorage = createLocalStorage();
  const networkId = await getNetworkId();
  const c = networkId + "_" + contractName;
  const addressOrNull = localStorage.getItem(c);
  return addressOrNull
    ? addressOrNull
    : Promise.reject("No address for " + contractName);
};

export const getNetworkId = async (): Promise<string> => {
  return (await ethers.provider.getNetwork()).chainId.toString();
};

export const getSigner = async (): Promise<Signer> => {
  return (await ethers.getSigners())[0];
};

export const getAddress = async (): Promise<string> => {
  return await (await getSigner()).getAddress();
};

export const getBalance = async (address: string): Promise<BigNumber> => {
  return await ethers.provider.getBalance(address);
};

export const getTransactionCount = async (address: string): Promise<number> => {
  return await ethers.provider.getTransactionCount(address);
};
