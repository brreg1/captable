import "@nomiclabs/buidler/types";

declare module "@nomiclabs/buidler/types" {
  export interface BuidlerConfig {
    deployed?: {
      [networkId: string]: {
        [contractName: string]: string;
      };
    };
  }

  export interface ResolvedBuidlerConfig {
    deployed: {
      [networkId: string]: {
        [contractName: string]: string;
      };
    };
  }
}
